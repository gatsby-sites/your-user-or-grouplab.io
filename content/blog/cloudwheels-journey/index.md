---
title: The cloudWheels journey
date: "2018-12-16T07:00:00.000Z"
---

Welcome to the cloudWheels tech blog. Here's a summary of who I am, what it's all about, and what's been happening recently.

##Who are you and what's this all about?
My name is **Nigel Wheeler** and I'm an individual based in the UK, so all views expressed here are my own. I like a lot of different things, so I've adopted the **cloudWheels** namespace as a home for my inner geek, and this blog as a place to share my thoughts and experiences with **software development**.

Plenty more to explain at a later date, but for now there's some work to be done...

## Latest updates
***December 16 2018***

So, as you can see, I've only just begun! This is a pretty simple blog site right now, and that's a good thing as I'd like to set out the steps I've been through to show just how simple it is, and that's not a bad place to start for a first proper post. 

Well, I say the first, but I'm really soooo excited about "what" I'm using to do this, I'll have to give that a quick mention too.

So, the opening players are:
* [**Crostini for Christmas**](/crostini-for-christmas/):  a gift from Google as **Linux on Chromebook goes beta**
* [**Deploying a Gatsby site to Gitlab pages**](/deploy-gatsby-static-site-to-gitlab-pages-free-hosting/): hosting a superfast static website for free